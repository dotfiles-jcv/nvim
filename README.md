# nvim dotfiles
## Getting started

This is a collection of my dotfiles for neovim. It should be copy into the folder `.config/nvim` in your home directory.

## Requirements

### Packer

Follow the next link [packer.nvim](https://github.com/wbthomason/packer.nvim) and follow the installation guide.
