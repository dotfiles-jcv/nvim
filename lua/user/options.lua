--- nvim default configuation file.

local opt = vim.opt

-- General
opt.fileencoding = "utf-8"      -- the encoding written to a file
opt.hidden = true               -- Enable background buffers
opt.splitbelow = true           -- Horizontal splits will automatically be below
opt.splitright = true           -- Vertical splits will automatically be to the right
opt.termguicolors = true        -- True color support
opt.mouse = "a"                 -- Enable your mouse
opt.clipboard = "unnamedplus"   -- Copy paste between vim and everything else
opt.updatetime = 300            -- Faster completion
opt.timeoutlen = 500            -- By default timeoutlen is 1000 ms
opt.scrolloff = 8               -- Make it so there are always eight lines below your cursor
opt.sidescrolloff = 8           -- Make it so there are always eight columns to the right of your cursor
opt.guifont = "monospace:h12"   -- the font used in graphical neovim applications

-- Search
opt.hlsearch = true       -- Highlight on search
opt.incsearch = true      -- Incremental search
opt.ignorecase = true     -- Ignore case
opt.smartcase = true      -- Smart case
opt.inccommand = "split"  -- Show the effect of a substitution command incrementally, as it is being typed

-- Status Line
opt.statusline = "%#StatusLine#%<%F%=%l/%L (%p%%) %#StatusLineNC#%m %#StatusLine#%r %#StatusLineNC#%h %#StatusLine#%="  -- Status line

-- Backup
opt.backup = false                                    -- Do not create a backup file before overwriting an existing file
opt.writebackup = false                               -- Do not write a backup file before overwriting an existing file
opt.swapfile = false                                  -- Do not write swapfile
opt.undofile = true                                   -- Enable persistent undo
opt.undodir = vim.fn.stdpath("config") .. "/undodir"  -- Set undo directory
opt.undolevels = 1000                                 -- Maximum number of changes that can be undone

-- Window
opt.winblend = 10     -- Transparency
opt.winminheight = 1  -- Minimum window size
opt.winminwidth = 1   -- Minimum window size

-- Tab
opt.tabstop = 2       -- tab
opt.softtabstop = 2   -- soft tab
opt.shiftwidth = 2    -- shift width
opt.expandtab = true  -- expand tab
opt.smarttab = true   -- smart tab

-- Indent
opt.autoindent = true   -- autoindent
opt.smartindent = true  -- smart autoindent
opt.cindent = true      -- C/C++ indent
opt.showtabline = 2     -- always show tabs

-- Line
opt.number = true
opt.relativenumber = false
opt.cursorline = true
opt.pumheight = 10                          -- pop up menu height
opt.showmode = false
opt.showcmd = false
opt.cmdheight = 1
opt.laststatus = 2
opt.signcolumn = "yes"
opt.wrap = false

-- List
opt.list = true
opt.listchars = "tab:»·,trail:·,nbsp:○,extends:◣,precedes:◢"

--- Highlight on red the trailing whitespaces
vim.cmd [[highlight ExtraWhitespace ctermbg=red guibg=red]]
vim.cmd [[highlight SpecialKey ctermfg=blue ctermbg=red guifg=blue guibg=red]]

-- Fold
opt.foldmethod = "expr"                       -- folding, set to "expr" for treesitter based folding
opt.foldexpr = "nvim_treesitter#foldexpr()"   -- set to "nvim_treesitter#foldexpr()" for treesitter based folding
opt.foldlevel = 99                            -- open all folds by default
opt.foldlevelstart = 99                       -- open all folds by default
opt.foldenable = false                        -- disable folding
opt.foldnestmax = 10                          -- 10 nested fold max
opt.foldminlines = 1                          -- minimum number of lines for a fold to be created
opt.foldtext = "v:lua.foldtext()"             -- set the text displayed for closed folds
opt.completeopt = { "menuone", "noselect" }   -- Completion options (for deoplete)
opt.conceallevel = 0                          -- so that `` is visible in markdown files

vim.cmd "set whichwrap+=<,>,[,],h,l"  -- move to next line with theses keys
vim.cmd [[set iskeyword+=-]]          -- treat dash separated words as a word text object"
